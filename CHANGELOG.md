# Changelog

## [1.0.0] - 2020-10-06

- Official version release

## [0.1.5] - 2020-10-04

- Replace default github branch

## [0.1.4] - 2020-09-27

- Remove plugins that can cause vscode exceptions

## [0.1.3] - 2020-05-28

- Add new extension

## [0.1.2] - 2020-05-28

- Update settings config

## [0.1.1] - 2020-05-28

- Update settings config

## [0.1.0] - 2020-05-27

- Initial release
