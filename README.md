# Vue Extension Pack

<p align="center">
<img src="https://cdn.jsdelivr.net/gh/vtrois/vue-extension-pack-vscode@1.0.0/icon.png" height="300">
</p>

## About

The Vue Extension Pack is the ultimate collection of extensions for working with Vue.js in VS Code. 🎉

## Install

1. Open [Visual Studio Code](https://code.visualstudio.com/)
2. Press `Ctrl+Shift+X` to open the Extensions tab
3. Type `Vue Extension Pack` to find the extension
4. Click the `Install` button

## Configuration

### Update settings config (settings.json)

```json
{
  "eslint.validate": ["javascript", "javascriptreact", "vue"],
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll": true
  },
  "[vue]": {
    "editor.defaultFormatter": "octref.vetur"
  },
  "[javascript]": {
    "editor.defaultFormatter": "vscode.typescript-language-features"
  },
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "vetur.format.defaultFormatter.js": "vscode-typescript",
  "javascript.format.semicolons": "remove",
  "javascript.format.insertSpaceBeforeFunctionParenthesis": true,
  "explorer.confirmDelete": false
}
```

### Create/update Eslint Config (.eslintrc.js)

```js
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "@vue/standard"],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
  },
};
```

### Create/update Prettier Config (.prettierrc)

```json
{
  "semi": false,
  "singleQuote": true
}
```

### Create/update EditorConfig (.editorconfig)

```
[*.{js,jsx,ts,tsx,vue}]
indent_style = space
indent_size = 2
trim_trailing_whitespace = true
insert_final_newline = true
```

## Extensions Included

- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) - Vue tooling for VS Code
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Integrates ESLint JavaScript into VS Code
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - Code formatter using prettier
- [Vue 2 Snippets](https://marketplace.visualstudio.com/items?itemName=hollowtree.vue-snippets) - A Vue.js 2 Extension
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) - EditorConfig Support for Visual Studio Code
- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2) - A customizable extension for colorizing matching brackets
- [Import Cost](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost) - Display import/require package size in the editor
- [Gitmoji](https://marketplace.visualstudio.com/items?itemName=Vtrois.gitmoji-vscode) - An emoji tool for your git commit messages

## Acknowledgement

Thanks to the contributors who inspired this project.

**Project:**

All credits goes to original authors of the above mentioned extensions.

## License

The code is available under the [MIT](https://github.com/vtrois/vue-extension-pack-vscode/blob/main/LICENSE) license.
